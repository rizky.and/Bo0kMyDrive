from django.db import models

class Articles(models.Model):
    Title = models.CharField(max_length=100)
    Content = models.TextField()
    date=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % self.Title