from django.shortcuts import render
from django.http import HttpResponseRedirect

def Articles(request):
    return render(request,'Articles/Articles.html')
