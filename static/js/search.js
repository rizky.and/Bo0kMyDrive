window.onload = function() {
    var catalog = [
        {
            "car":"toyota",
            "type":"yaris",
            "rating":"4.7/5",
            "owner":"Michael Muhammad",
            "year":"2017",
            "form":"Hatchback",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Yaris.png",
            "price":"201.000"
        },
        {
            "car":"toyota",
            "type":"yaris",
            "rating":"4.5/5",
            "owner":"Athiya Fitri",
            "year":"2017",
            "form":"Hatchback",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Yaris2.png",
            "price":"240.000"
        },
        {
            "car":"toyota",
            "type":"yaris",
            "rating":"4.6/5",
            "owner":"Sumi Fukri",
            "year":"2018",
            "form":"Hatchback",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Yaris3.png",
            "price":"200.000"
        },
        {
            "car":"toyota",
            "type":"yaris",
            "rating":"4.3/5",
            "owner":"Joko Alin",
            "year":"2018",
            "form":"Hatchback",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Yaris4.png",
            "price":"289.000"
        },
        {
            "car":"toyota",
            "type":"yaris",
            "rating":"4.8/5",
            "owner":"Sonya Aminah",
            "year":"2017",
            "form":"Hatchback",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Yaris5.png",
            "price":"243.000"
        },
        {
            "car":"nissan",
            "type":"serena",
            "rating":"4.7/5",
            "owner":"Alianda Tasya",
            "year":"2018",
            "form":"MPV",
            "seat":"7-Seater",
            "door":"5",
            "pic": "/static/img/Serena.png",
            "price":"305.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.6/5",
            "owner":"Raja Situmorang",
            "year":"2017",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke.png",
            "price":"226.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.8/5",
            "owner":"Gifarie Fong",
            "year":"2017",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke2.png",
            "price":"297.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.5/5",
            "owner":"Raja Manunggrah",
            "year":"2018",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke3.png",
            "price":"211.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.0/5",
            "owner":"Alif Fadri",
            "year":"2019",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke4.png",
            "price":"287.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.7/5",
            "owner":"John Sitomang",
            "year":"2017",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke5.png",
            "price":"219.000"
        },
        {
            "car":"nissan",
            "type":"juke",
            "rating":"4.3/5",
            "owner":"Joko Widodo",
            "year":"2017",
            "form":"SUV",
            "seat":"5-Seater",
            "door":"5",
            "pic": "/static/img/Juke6.png",
            "price":"226.000"
        },
        {
            "car":"mitsubishi",
            "type":"xpander",
            "rating":"4.5/5",
            "owner":"Joko Sarwono",
            "year":"2019",
            "form":"MPV",
            "seat":"7-Seater",
            "door":"5",
            "pic": "/static/img/Xpander.png",
            "price":"194.000"
        },
        {
            "car":"mitsubishi",
            "type":"xpander",
            "rating":"5.0/5",
            "owner":"Aminah Susanti",
            "year":"2017",
            "form":"MPV",
            "seat":"7-Seater",
            "door":"5",
            "pic": "/static/img/Xpander2.png",
            "price":"290.000"
        },
        {
            "car":"mitsubishi",
            "type":"xpander",
            "rating":"4.6/5",
            "owner":"X Æ A-12",
            "year":"2017",
            "form":"MPV",
            "seat":"7-Seater",
            "door":"5",
            "pic": "/static/img/Xpander3.png",
            "price":"205.000"
        },
    ];
    var place = document.getElementById("cars");
    var btn = document.getElementById("searchtype");
    var btn2 = document.getElementById("searchprice");

    btn.addEventListener("click",function(){
        var value = document.getElementById("typesubmit").value;
        renderHTMLtype(value);
    })

    btn2.addEventListener("click",function(){
        var value = document.getElementById("pricesubmit").value;
        renderHTMLprice(value);
    })

    function renderHTMLtype(value) {
        var numb = 0;
        var HTMLstr = "<div class='row align-items-end'>";
        for (var i=0; i < catalog.length;i++) {
            if(numb % 4 == 0 && numb > 0){
                HTMLstr += "</div><br><br><div class='row align-items-end'>";
            }
            if(value==catalog[i].type){
                numb+= 1
                const img = catalog[i].pic
                const car = catalog[i].car
                const type = catalog[i].type
                const rating = catalog[i].rating
                const owner = catalog[i].owner
                const year = catalog[i].year
                const form = catalog[i].form
                const seat = catalog[i].seat
                const door = catalog[i].door
                const price = catalog[i].price

                HTMLstr += "<div class='col-lg'>"+
                "<img src=\""+img+"\"><br>"+
                "<p class='fontz'>"+
                "<b style='text-transform:uppercase;'>"+car+"&nbsp;"+type+"</b><br>"+
                ""+rating+"<br>"+
                "<b>Owner:</b>"+owner+"<br>"+
                "<b>Year:</b>"+year+"<br>"+
                "<b>Type:</b>"+form+"<br><br>"+
                "<b>Seat:</b>"+seat+"<br>"+
                "<b>Door:</b>"+door+"<br><br>"+
                "<div style='font-size: 30px;'><b>Rp "+price+"</b>/day</div></p></div>"
            }
        }
        HTMLstr += "</div>"
        place.innerHTML = HTMLstr;
    }

    function renderHTMLprice(value) {
        var numb = 0;
        var HTMLstr = "<div class='row align-items-center'>";
        for (var i=0; i < catalog.length;i++) {
            if(numb != 0 && numb % 4 == 0){
                HTMLstr += "</div><br><br><div class='row align-items-center'>";
            }
            if(value > parseInt(catalog[i].price.replace(".", ""))){
                numb+= 1
                const img = catalog[i].pic
                const car = catalog[i].car
                const type = catalog[i].type
                const rating = catalog[i].rating
                const owner = catalog[i].owner
                const year = catalog[i].year
                const form = catalog[i].form
                const seat = catalog[i].seat
                const door = catalog[i].door
                const price = catalog[i].price

                HTMLstr += "<div class='col'>"+
                "<img src=\""+img+"\"><br>"+
                "<p class='fontz'>"+
                "<b style='text-transform:uppercase;'>"+car+"&nbsp;"+type+"</b><br>"+
                ""+rating+"<br>"+
                "<b>Owner:</b>"+owner+"<br>"+
                "<b>Year:</b>"+year+"<br>"+
                "<b>Type:</b>"+form+"<br><br>"+
                "<b>Seat:</b>"+seat+"<br>"+
                "<b>Door:</b>"+door+"<br><br>"+
                "<div style='font-size: 30px;'><b>Rp "+price+"</b>/day</div></p></div>"
            }
        }
        HTMLstr += "</div>"
        place.innerHTML = HTMLstr;
    }
}