from django.urls import path
from .views import AddReviews

urlpatterns = [
    path('', AddReviews,name='Reviews'),
]