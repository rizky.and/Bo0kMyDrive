from django.shortcuts import render
from .forms import CreateReviews
from .models import Reviews
from django.http import HttpResponseRedirect

def AddReviews(request):
    AddReviews=Reviews.objects.all().order_by('-date')
    if request.method=="POST":
        form=CreateReviews(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/Reviews')
    else:
        form=CreateReviews()
    
    return render(request,'Reviews/Reviews.html',{'form':form, 'Reviews':AddReviews})