from django.db import models

class Reviews(models.Model):
    ORDER_NO = models.CharField(max_length=100)
    OWNER = models.CharField(max_length=100)
    CAR = models.CharField(max_length=100)
    YOUR_NAME= models.CharField(max_length=100)
    EMAIL=models.EmailField()
    REVIEWS = models.TextField()
    Rating_Choices=(("1","The Worst!"),("2","Bad"),("3","OK"),("4","Good"),("5","Awesome!"))
    RATINGS = models.CharField(choices=Rating_Choices,default="1",null="True",max_length=5)
    date=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % self.YOUR_NAME
    
