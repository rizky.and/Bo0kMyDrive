from django.urls import path
from .views import FavoriteCar

urlpatterns = [
    path('', FavoriteCar,name='FavoriteCar'),
]