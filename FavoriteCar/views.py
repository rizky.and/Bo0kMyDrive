from django.shortcuts import render
from django.http import HttpResponseRedirect

def FavoriteCar(request):
    return render(request,'FavoriteCar.html')
