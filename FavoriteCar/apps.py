from django.apps import AppConfig


class FavoritecarConfig(AppConfig):
    name = 'FavoriteCar'
